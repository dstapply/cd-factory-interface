package hci.project4.views;

import hci.project4.entities.MusicCD;
import hci.project4.entities.User;
import hci.project4.entities.UserController;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.Spring;
import javax.swing.SpringLayout;

@SuppressWarnings("serial")
public class CDPanel extends JPanel implements ActionListener {
	private static CDPanel instance = null;
	private SpringLayout mainLayout;
	private BufferedImage logoImage;
	private JLabel logoLabel;
	private JButton homeButton, buyButton, addToWishListButton;
	private JLabel titleLabel, artistLabel, albumLabel, genreLabel, costLabel;
	private JLabel cdTitleLabel, cdArtistLabel, cdAlbumLabel, cdGenreLabel, cdCostLabel;
	private MusicCD cd;
	private JLabel browseLabel;
	private JButton genreBCButton;
	private JLabel carrotLabel1;
	private JButton artistBCButton;
	private JLabel carrotLabel2;
	private JButton albumBCButton;
	
	private UserController userController = UserController.getInstance();

	
	public static CDPanel getInstance() {
		if (instance == null) {
			instance = new CDPanel(new MusicCD());
		}
		return instance;
	}
	
	private CDPanel(MusicCD cd) {
		super();
		this.cd = cd;
		mainLayout = new SpringLayout();
		setLayout(mainLayout);
		
		Font labelFont = new Font("Verdana", Font.BOLD, 30);
		Font cdLabelFont = new Font("Verdana", Font.PLAIN, 30);
		Font textFieldFont = new Font("Verdana", Font.PLAIN, 24);
		Font buttonFont = new Font("Verdana", Font.PLAIN, 20);
		Font homeButtonFont = new Font("Verdana", Font.PLAIN, 42);
		Font bcLabelFont = new Font("Verdana", Font.PLAIN, 24);
		
		//Logo
		try {
			logoImage = ImageIO.read(new File("src/images/smallLogo.png"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		logoLabel = new JLabel(new ImageIcon(logoImage));
		SpringLayout.Constraints logoLabelCons = mainLayout.getConstraints(logoLabel);
		logoLabelCons.setX(Spring.constant(30));
		logoLabelCons.setY(Spring.constant(30));
		add(logoLabel);
		
		// Home Button
		homeButton = new JButton("Home");
		homeButton.setFont(homeButtonFont);
		homeButton.setActionCommand("home");
		homeButton.addActionListener(this);
		homeButton.setPreferredSize(new Dimension(210, 80));
		SpringLayout.Constraints homeButtonCons = mainLayout.getConstraints(homeButton);
		homeButtonCons.setX(Spring.constant(1000));
		homeButtonCons.setY(Spring.constant(50));
		add(homeButton);
		
		// CD info
		albumLabel = new JLabel("Album: ");
		albumLabel.setFont(labelFont);
		SpringLayout.Constraints albumLabelCons = mainLayout.getConstraints(albumLabel);
		albumLabelCons.setX(Spring.constant(480));
		albumLabelCons.setY(Spring.constant(350));
		add(albumLabel);
		
		cdAlbumLabel = new JLabel(cd.getAlbum());
		cdAlbumLabel.setFont(cdLabelFont);
		SpringLayout.Constraints cdAlbumLabelCons = mainLayout.getConstraints(cdAlbumLabel);
		cdAlbumLabelCons.setX(Spring.constant(610));
		cdAlbumLabelCons.setY(Spring.constant(350));
		add(cdAlbumLabel);
		
		artistLabel = new JLabel("Artist: ");
		artistLabel.setFont(labelFont);
		SpringLayout.Constraints artistLabelCons = mainLayout.getConstraints(artistLabel);
		artistLabelCons.setX(Spring.constant(492));
		artistLabelCons.setY(Spring.constant(300));
		add(artistLabel);
		
		cdArtistLabel = new JLabel(cd.getArtist());
		cdArtistLabel.setFont(cdLabelFont);
		SpringLayout.Constraints cdArtistLabelCons = mainLayout.getConstraints(cdArtistLabel);
		cdArtistLabelCons.setX(Spring.constant(610));
		cdArtistLabelCons.setY(Spring.constant(300));
		add(cdArtistLabel);
		
		genreLabel = new JLabel("Genre: ");
		genreLabel.setFont(labelFont);
		SpringLayout.Constraints genreLabelCons = mainLayout.getConstraints(genreLabel);
		genreLabelCons.setX(Spring.constant(486));
		genreLabelCons.setY(Spring.constant(400));
		add(genreLabel);
		
		cdGenreLabel = new JLabel(cd.getGenre());
		cdGenreLabel.setFont(cdLabelFont);
		SpringLayout.Constraints cdGenreLabelCons = mainLayout.getConstraints(cdGenreLabel);
		cdGenreLabelCons.setX(Spring.constant(610));
		cdGenreLabelCons.setY(Spring.constant(400));
		add(cdGenreLabel);

		costLabel = new JLabel("Cost: ");
		costLabel.setFont(labelFont);
		SpringLayout.Constraints costLabelCons = mainLayout.getConstraints(costLabel);
		costLabelCons.setX(Spring.constant(510));
		costLabelCons.setY(Spring.constant(450));
		add(costLabel);
		
		cdCostLabel = new JLabel(cd.getCostInDollars());
		cdCostLabel.setFont(cdLabelFont);
		SpringLayout.Constraints cdCostLabelCons = mainLayout.getConstraints(cdCostLabel);
		cdCostLabelCons.setX(Spring.constant(610));
		cdCostLabelCons.setY(Spring.constant(450));
		add(cdCostLabel);
		
		// Buy / Add to wishlist
		buyButton = new JButton("Buy");
		buyButton.setFont(buttonFont);
		buyButton.setActionCommand("buy");
		buyButton.addActionListener(this);
		buyButton.setPreferredSize(new Dimension(150, 40));
		SpringLayout.Constraints buyButtonCons = mainLayout.getConstraints(buyButton);
		buyButtonCons.setX(Spring.constant(470));
		buyButtonCons.setY(Spring.constant(570));
		add(buyButton);
		
		addToWishListButton = new JButton("Add to Wish List");
		addToWishListButton.setFont(buttonFont);
		addToWishListButton.setActionCommand("addToWL");
		addToWishListButton.addActionListener(this);
		addToWishListButton.setPreferredSize(new Dimension(220, 40));
		SpringLayout.Constraints addToWishListButtonCons = mainLayout.getConstraints(addToWishListButton);
		addToWishListButtonCons.setX(Spring.constant(650));
		addToWishListButtonCons.setY(Spring.constant(570));
		add(addToWishListButton);
		addToWishListButton.setVisible(false);
		
		// Browse Bread Crumbs
		browseLabel = new JLabel("Browse: ");
		browseLabel.setFont(bcLabelFont);
		SpringLayout.Constraints browseLabelCons = mainLayout.getConstraints(browseLabel);
		browseLabelCons.setX(Spring.constant(100));
		browseLabelCons.setY(Spring.constant(250));
		add(browseLabel);
		
		genreBCButton = new JButton("Genres");
		genreBCButton.setFont(bcLabelFont);
		genreBCButton.setForeground(Color.blue);
		genreBCButton.setFocusPainted(false);
		genreBCButton.setMargin(new Insets(0, 0, 0, 0));
		genreBCButton.setContentAreaFilled(false);
		genreBCButton.setBorderPainted(false);
        genreBCButton.setOpaque(false);
        genreBCButton.setActionCommand("genrebc");
        genreBCButton.addActionListener(this);
		SpringLayout.Constraints genreBCButtonCons = mainLayout.getConstraints(genreBCButton);
		genreBCButtonCons.setX(Spring.constant(210));
		genreBCButtonCons.setY(Spring.constant(250));
		add(genreBCButton);
		
		carrotLabel1 = new JLabel(">");
		carrotLabel1.setFont(bcLabelFont);
		SpringLayout.Constraints carrot1LabelCons = mainLayout.getConstraints(carrotLabel1);
		carrot1LabelCons.setX(Spring.constant(305));
		carrot1LabelCons.setY(Spring.constant(253));
		add(carrotLabel1);
		
		artistBCButton = new JButton("Artists");
		artistBCButton.setFont(bcLabelFont);
		artistBCButton.setForeground(Color.blue);
		artistBCButton.setFocusPainted(false);
		artistBCButton.setMargin(new Insets(0, 0, 0, 0));
		artistBCButton.setContentAreaFilled(false);
		artistBCButton.setBorderPainted(false);
		artistBCButton.setOpaque(false);
		artistBCButton.setActionCommand("artistbc");
		artistBCButton.addActionListener(this);
		SpringLayout.Constraints artistBCButtonCons = mainLayout.getConstraints(artistBCButton);
		artistBCButtonCons.setX(Spring.constant(328));
		artistBCButtonCons.setY(Spring.constant(250));
		add(artistBCButton);
		
		carrotLabel2 = new JLabel(">");
		carrotLabel2.setFont(bcLabelFont);
		SpringLayout.Constraints carrotLabel2Cons = mainLayout.getConstraints(carrotLabel2);
		carrotLabel2Cons.setX(Spring.constant(412));
		carrotLabel2Cons.setY(Spring.constant(253));
		add(carrotLabel2);
		
		albumBCButton = new JButton("Albums");
		albumBCButton.setFont(bcLabelFont);
		albumBCButton.setForeground(Color.blue);
		albumBCButton.setFocusPainted(false);
		albumBCButton.setMargin(new Insets(0, 0, 0, 0));
		albumBCButton.setContentAreaFilled(false);
		albumBCButton.setBorderPainted(false);
		albumBCButton.setOpaque(false);
		albumBCButton.setActionCommand("albumbc");
		albumBCButton.addActionListener(this);
		SpringLayout.Constraints albumBCButtonCons = mainLayout.getConstraints(albumBCButton);
		albumBCButtonCons.setX(Spring.constant(432));
		albumBCButtonCons.setY(Spring.constant(250));
		add(albumBCButton);
	}
	
	public void setCD(MusicCD cd) {
		this.cd = cd;
		
		//Update info
		cdArtistLabel.setText(cd.getArtist());
		cdAlbumLabel.setText(cd.getAlbum());
		cdGenreLabel.setText(cd.getGenre());
		cdCostLabel.setText(cd.getCostInDollars());
		
		repaint();
		revalidate();
	}
	
	public void setAddToWishListButtonVisible(boolean val) {
		addToWishListButton.setVisible(val);
	}
	
	@Override
	public void actionPerformed(ActionEvent action) {
		if (action.getActionCommand().equals("home")) {
			this.setVisible(false);
			this.getParent().getComponent(0).setVisible(true);
		} else if (action.getActionCommand().equals("addToWL")) {
			User currentUser = UserController.getInstance().getLoggedInUser();
			if (currentUser != null) {
				if (!currentUser.getWishList().contains(cd)) {
					currentUser.addCDToWishList(cd);
					WishListPanel.getInstance().updateWishListTable();
				} else {
					// CD already in wish list
					System.err.println("CD already in users wish list");
				}
			} else {
				// No user logged in
				System.err.println("No user logged in to add cd to wish list");
			}
		} else if (action.getActionCommand().equals("genrebc")) {
			BrowsePanel.getInstance().goToBrowseState(BrowseState.GENRE_STATE, "");
			this.setVisible(false);
			this.getParent().getComponent(5).setVisible(true);
		} else if (action.getActionCommand().equals("artistbc")) {
			BrowsePanel.getInstance().setCurrentGenre(cd.getGenre());
			BrowsePanel.getInstance().goToBrowseState(BrowseState.ARTIST_STATE, cd.getGenre());
			this.setVisible(false);
			this.getParent().getComponent(5).setVisible(true);
		} else if (action.getActionCommand().equals("albumbc")) {
			BrowsePanel.getInstance().setCurrentGenre(cd.getGenre());
			BrowsePanel.getInstance().setCurrentArtist(cd.getArtist());
			BrowsePanel.getInstance().goToBrowseState(BrowseState.ALBUM_STATE, cd.getArtist());
			this.setVisible(false);
			this.getParent().getComponent(5).setVisible(true);
		}
	}
}
