package hci.project4.views;

import hci.project4.entities.Catalog;
import hci.project4.entities.LoginAuthCodes;
import hci.project4.entities.MusicCD;
import hci.project4.entities.UserController;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.Spring;
import javax.swing.SpringLayout;

enum BrowseState {
	GENRE_STATE,
	ARTIST_STATE,
	ALBUM_STATE;
}

@SuppressWarnings("serial")
public class BrowsePanel extends JPanel implements ActionListener {
	private static BrowsePanel instance = null;
	private SpringLayout mainLayout;
	private BufferedImage logoImage;
	private JLabel logoLabel, browseLabel, carrotLabel1, carrotLabel2;
	private JButton homeButton, genreBCButton, artistBCButton, albumBCButton;
	private ArrayList<JButton> genres, artists, albums;
	private Font buttonFont, labelFont, textFieldFont, homeButtonFont;
	private String currentGenre, currentArtist;
	
	private UserController userController = UserController.getInstance();
	
	public static BrowsePanel getInstance() {
		if (instance == null) {
			instance = new BrowsePanel();
		}
		return instance;
	}
	
	private BrowsePanel() {
		super();
		mainLayout = new SpringLayout();
		setLayout(mainLayout);
		
		labelFont = new Font("Verdana", Font.PLAIN, 24);
		textFieldFont = new Font("Verdana", Font.PLAIN, 24);
		buttonFont = new Font("Verdana", Font.PLAIN, 20);
		homeButtonFont = new Font("Verdana", Font.PLAIN, 42);
		
		genres = new ArrayList<JButton>();
		artists = new ArrayList<JButton>();
		albums = new ArrayList<JButton>();
		
		//Logo
		try {
			logoImage = ImageIO.read(new File("src/images/smallLogo.png"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		logoLabel = new JLabel(new ImageIcon(logoImage));
		SpringLayout.Constraints logoLabelCons = mainLayout.getConstraints(logoLabel);
		logoLabelCons.setX(Spring.constant(30));
		logoLabelCons.setY(Spring.constant(30));
		add(logoLabel);
		
		// Home Button
		homeButton = new JButton("Home");
		homeButton.setFont(homeButtonFont);
		homeButton.setActionCommand("home");
		homeButton.addActionListener(this);
		homeButton.setPreferredSize(new Dimension(210, 80));
		SpringLayout.Constraints homeButtonCons = mainLayout.getConstraints(homeButton);
		homeButtonCons.setX(Spring.constant(1000));
		homeButtonCons.setY(Spring.constant(50));
		add(homeButton);
		
		browseLabel = new JLabel("Browse: ");
		browseLabel.setFont(labelFont);
		SpringLayout.Constraints browseLabelCons = mainLayout.getConstraints(browseLabel);
		browseLabelCons.setX(Spring.constant(100));
		browseLabelCons.setY(Spring.constant(250));
		add(browseLabel);
		
		genreBCButton = new JButton("Genres");
		genreBCButton.setFont(labelFont);
		genreBCButton.setForeground(Color.blue);
		genreBCButton.setFocusPainted(false);
		genreBCButton.setMargin(new Insets(0, 0, 0, 0));
		genreBCButton.setContentAreaFilled(false);
		genreBCButton.setBorderPainted(false);
        genreBCButton.setOpaque(false);
        genreBCButton.setActionCommand("genrebc");
        genreBCButton.addActionListener(this);
        genreBCButton.setVisible(false);
		SpringLayout.Constraints genreBCButtonCons = mainLayout.getConstraints(genreBCButton);
		genreBCButtonCons.setX(Spring.constant(210));
		genreBCButtonCons.setY(Spring.constant(250));
		add(genreBCButton);
		
		carrotLabel1 = new JLabel(">");
		carrotLabel1.setFont(labelFont);
		carrotLabel1.setVisible(false);
		SpringLayout.Constraints carrot1LabelCons = mainLayout.getConstraints(carrotLabel1);
		carrot1LabelCons.setX(Spring.constant(305));
		carrot1LabelCons.setY(Spring.constant(253));
		add(carrotLabel1);
		
		artistBCButton = new JButton("Artists");
		artistBCButton.setFont(labelFont);
		artistBCButton.setForeground(Color.blue);
		artistBCButton.setFocusPainted(false);
		artistBCButton.setMargin(new Insets(0, 0, 0, 0));
		artistBCButton.setContentAreaFilled(false);
		artistBCButton.setBorderPainted(false);
		artistBCButton.setOpaque(false);
		artistBCButton.setActionCommand("artistbc");
		artistBCButton.addActionListener(this);
		artistBCButton.setVisible(false);
		SpringLayout.Constraints artistBCButtonCons = mainLayout.getConstraints(artistBCButton);
		artistBCButtonCons.setX(Spring.constant(328));
		artistBCButtonCons.setY(Spring.constant(250));
		add(artistBCButton);
		
		carrotLabel2 = new JLabel(">");
		carrotLabel2.setFont(labelFont);
		carrotLabel2.setVisible(false);
		SpringLayout.Constraints carrotLabel2Cons = mainLayout.getConstraints(carrotLabel2);
		carrotLabel2Cons.setX(Spring.constant(412));
		carrotLabel2Cons.setY(Spring.constant(253));
		add(carrotLabel2);
		
		albumBCButton = new JButton("Albums");
		albumBCButton.setFont(labelFont);
		albumBCButton.setForeground(Color.blue);
		albumBCButton.setFocusPainted(false);
		albumBCButton.setMargin(new Insets(0, 0, 0, 0));
		albumBCButton.setContentAreaFilled(false);
		albumBCButton.setBorderPainted(false);
		albumBCButton.setOpaque(false);
		albumBCButton.setActionCommand("albumbc");
		albumBCButton.addActionListener(this);
		albumBCButton.setVisible(false);
		SpringLayout.Constraints albumBCButtonCons = mainLayout.getConstraints(albumBCButton);
		albumBCButtonCons.setX(Spring.constant(432));
		albumBCButtonCons.setY(Spring.constant(250));
		add(albumBCButton);
		
	}
	
	public void goToBrowseState(BrowseState state, String selectedCategory) {
		int currentButtonX = 0;
		int currentButtonY = 0;
		switch(state) {
		case GENRE_STATE:
			System.out.println("Entering Genre State");
			for (int i = 0; i < artists.size(); i++) {
				remove(artists.get(i));
			}
			for (int i = 0; i < albums.size(); i++) {
				remove(albums.get(i));
			}
			for (int i = 0; i < genres.size(); i++) {
				remove(genres.get(i));
			}
			currentGenre = "";
			currentArtist = "";
			ArrayList<String> genreStrings = Catalog.getInstance().getAllGenres();
			genres.clear();
			for (int i = 0; i < genreStrings.size(); i++) {
				genres.add(new JButton(genreStrings.get(i)));
				genres.get(i).setActionCommand(genreStrings.get(i).toLowerCase());
				genres.get(i).addActionListener(this);
				genres.get(i).setFont(buttonFont);
				genres.get(i).setPreferredSize(new Dimension(150, 80));
			}
			
			currentButtonX = 150;
			currentButtonY = 350;
			for (int i = 0; i < genres.size(); i++) {
				SpringLayout.Constraints genreButtonCons = mainLayout.getConstraints(genres.get(i));
				genreButtonCons.setX(Spring.constant(currentButtonX));
				genreButtonCons.setY(Spring.constant(currentButtonY));
				add(genres.get(i));
				
				currentButtonX += 200;
				if (currentButtonX%1000 == 150) {
					currentButtonY += 150;
					currentButtonX = 150;
				}
			}
			genreBCButton.setVisible(false);
			carrotLabel1.setVisible(false);
			artistBCButton.setVisible(false);
			carrotLabel2.setVisible(false);
			albumBCButton.setVisible(false);
			break;
		case ARTIST_STATE:
			System.out.println("Entering Artist State");
			for (int i = 0; i < genres.size(); i++) {
				remove(genres.get(i));
			}
			for (int i = 0; i < albums.size(); i++) {
				remove(albums.get(i));
			}
			for (int i = 0; i < artists.size(); i++) {
				remove(artists.get(i));
			}
			currentArtist = "";
			ArrayList<String> artistStrings = Catalog.getInstance().getArtistsByGenre(selectedCategory);
			artists.clear();
			for (int i = 0; i < artistStrings.size(); i++) {
				artists.add(new JButton(artistStrings.get(i)));
				artists.get(i).setActionCommand(artistStrings.get(i).toLowerCase());
				artists.get(i).addActionListener(this);
				artists.get(i).setFont(buttonFont);
				artists.get(i).setPreferredSize(new Dimension(150, 80));
			}
			
			currentButtonX = 150;
			currentButtonY = 350;
			for (int i = 0; i < artists.size(); i++) {
				SpringLayout.Constraints artistButtonCons = mainLayout.getConstraints(artists.get(i));
				artistButtonCons.setX(Spring.constant(currentButtonX));
				artistButtonCons.setY(Spring.constant(currentButtonY));
				add(artists.get(i));
				
				currentButtonX += 200;
				if (currentButtonX%1000 == 150) {
					currentButtonY += 150;
					currentButtonX = 150;
				}
			}
			genreBCButton.setVisible(true);
			carrotLabel1.setVisible(false);
			artistBCButton.setVisible(false);
			carrotLabel2.setVisible(false);
			albumBCButton.setVisible(false);
			break;
		case ALBUM_STATE:
			System.out.println("Entering Album State");
			for (int i = 0; i < genres.size(); i++) {
				remove(genres.get(i));
			}
			for (int i = 0; i < artists.size(); i++) {
				remove(artists.get(i));
			}
			for (int i = 0; i < albums.size(); i++) {
				remove(albums.get(i));
			}
			ArrayList<String> albumStrings = Catalog.getInstance().getAlbumsByArtist(selectedCategory);
			albums.clear();
			for (int i = 0; i < albumStrings.size(); i++) {
				albums.add(new JButton(albumStrings.get(i)));
				albums.get(i).setActionCommand(albumStrings.get(i).toLowerCase());
				albums.get(i).addActionListener(this);
				albums.get(i).setFont(buttonFont);
				albums.get(i).setPreferredSize(new Dimension(150, 80));
			}
			
			currentButtonX = 150;
			currentButtonY = 350;
			for (int i = 0; i < albums.size(); i++) {
				SpringLayout.Constraints albumButtonCons = mainLayout.getConstraints(albums.get(i));
				albumButtonCons.setX(Spring.constant(currentButtonX));
				albumButtonCons.setY(Spring.constant(currentButtonY));
				add(albums.get(i));
				
				currentButtonX += 200;
				if (currentButtonX%1000 == 150) {
					currentButtonY += 150;
					currentButtonX = 150;
				}
			}
			genreBCButton.setVisible(true);
			carrotLabel1.setVisible(true);
			artistBCButton.setVisible(true);
			carrotLabel2.setVisible(false);
			albumBCButton.setVisible(false);
			break;
		default:
			//not possible?
			System.err.println("Error Browse State");
			break;
		}
		repaint();
		revalidate();
	}
	
	private void goToCDPanel(MusicCD cd) {
		this.setVisible(false);
		CDPanel.getInstance().setCD(cd);
		CDPanel.getInstance().setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent action) {
		if (action.getActionCommand().equals("home")) {
			this.setVisible(false);
			this.getParent().getComponent(0).setVisible(true);
		} else if (action.getActionCommand().equals("genrebc")) {
			goToBrowseState(BrowseState.GENRE_STATE, "");
		} else if (action.getActionCommand().equals("artistbc")) {
			goToBrowseState(BrowseState.ARTIST_STATE, currentGenre);
		} else if (action.getActionCommand().equals("albumbc")) {
			goToBrowseState(BrowseState.ALBUM_STATE, currentArtist);
		}
		for (int i = 0; i < genres.size(); i++) {
			if (action.getActionCommand().equals(genres.get(i).getActionCommand())) {
				currentGenre = genres.get(i).getText();
				goToBrowseState(BrowseState.ARTIST_STATE, currentGenre);
				break;
			}
		}
		for (int i = 0; i < artists.size(); i++) {
			if (action.getActionCommand().equals(artists.get(i).getActionCommand())) {
				currentArtist = artists.get(i).getText();
				goToBrowseState(BrowseState.ALBUM_STATE, currentArtist);
				break;
			}
		}
		for (int i = 0; i < albums.size(); i++) {
			if (action.getActionCommand().equals(albums.get(i).getActionCommand())) {
				// Go to that cd
				goToCDPanel(Catalog.getInstance().getCD(albums.get(i).getText(), currentArtist));
				break;
			}
		}
	}
	
	public String getCurrentGenre() {
		return currentGenre;
	}
	
	public void setCurrentGenre(String genre) {
		currentGenre = genre;
	}
	
	public String getCurrentArtist() {
		return currentArtist;
	}
	
	public void setCurrentArtist(String artist) {
		currentArtist = artist;
	}
	
}
