package hci.project4.views;

import hci.project4.entities.Catalog;
import hci.project4.entities.LoginAuthCodes;
import hci.project4.entities.MusicCD;
import hci.project4.entities.UserController;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.Spring;
import javax.swing.SpringLayout;
import javax.swing.table.DefaultTableModel;

@SuppressWarnings("serial")
public class WishListPanel extends JPanel implements ActionListener {
	private static WishListPanel instance;
	private SpringLayout mainLayout;
	private BufferedImage logoImage;
	private JLabel logoLabel, wishListTableLabel;
	private JButton homeButton, buyButton, removeButton;
	private JScrollPane tablePane;
	private JTable wishListTable;
	private Vector<String> columnNames;
	private Vector<Vector<String>> tableData;
	private Catalog catalog = Catalog.getInstance();
	
	private UserController userController = UserController.getInstance();
	
	public static WishListPanel getInstance() {
		if (instance == null) {
			instance = new WishListPanel();
		}
		return instance;
	}
	
	private WishListPanel() {
		super();
		tablePane = new JScrollPane();
		mainLayout = new SpringLayout();
		setLayout(mainLayout);
		
		Font labelFont = new Font("Verdana", Font.BOLD, 24);
		Font textFieldFont = new Font("Verdana", Font.PLAIN, 24);
		Font buttonFont = new Font("Verdana", Font.PLAIN, 20);
		Font homeButtonFont = new Font("Verdana", Font.PLAIN, 42);
		Font tableFont = new Font("Verdana", Font.PLAIN, 16);
		Font tableLabelFont = new Font("Verdana", Font.PLAIN, 30);
		
		//Logo
		try {
			logoImage = ImageIO.read(new File("src/images/smallLogo.png"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		logoLabel = new JLabel(new ImageIcon(logoImage));
		SpringLayout.Constraints logoLabelCons = mainLayout.getConstraints(logoLabel);
		logoLabelCons.setX(Spring.constant(30));
		logoLabelCons.setY(Spring.constant(30));
		add(logoLabel);
		
		// Home Button
		homeButton = new JButton("Home");
		homeButton.setFont(homeButtonFont);
		homeButton.setActionCommand("home");
		homeButton.addActionListener(this);
		homeButton.setPreferredSize(new Dimension(210, 80));
		SpringLayout.Constraints homeButtonCons = mainLayout.getConstraints(homeButton);
		homeButtonCons.setX(Spring.constant(1000));
		homeButtonCons.setY(Spring.constant(50));
		add(homeButton);
		
		wishListTableLabel = new JLabel("Wish List");
		wishListTableLabel.setFont(tableLabelFont);
		SpringLayout.Constraints tableLabelCons = mainLayout.getConstraints(wishListTableLabel);
		tableLabelCons.setX(Spring.constant(550));
		tableLabelCons.setY(Spring.constant(200));
		add(wishListTableLabel);
		
		// CD Table
		columnNames = new Vector<String>();
		columnNames.add(0, "#");
		columnNames.add(1, "Album");
		columnNames.add(2, "Artist");
		columnNames.add(3, "Genre");
		columnNames.add(4, "Cost");
		
		tableData = new Vector<Vector<String>>();
		
		DefaultTableModel tableModel = new DefaultTableModel(tableData, columnNames) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		
		wishListTable = new JTable(tableModel);
		wishListTable.getColumnModel().getColumn(0).setMaxWidth(50);
		wishListTable.getColumnModel().getColumn(1).setMinWidth(230);
		wishListTable.getColumnModel().getColumn(2).setMinWidth(150);
		wishListTable.getColumnModel().getColumn(4).setMaxWidth(80);
		wishListTable.getColumnModel().getColumn(4).setMinWidth(80);
		wishListTable.setFont(tableFont);
		wishListTable.setRowHeight(30);
		
		wishListTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		wishListTable.addMouseListener(new MouseAdapter() {
		    public void mousePressed(MouseEvent me) {
		        JTable table =(JTable) me.getSource();
		        Point p = me.getPoint();
		        int row = table.rowAtPoint(p);
		        if (me.getClickCount() == 2) {
		        	MusicCD cd;
		        	if ((cd = catalog.getCD((String)table.getValueAt(row, 1), (String)table.getValueAt(row, 2))) != null) {
		        		goToCDPanel(cd);
		        	} else {
		        		//cd not found
		        	}
		        }
		    }
		});
		
		tablePane.setPreferredSize(new Dimension(800, 323));
		
		SpringLayout.Constraints wishListTableCons = mainLayout.getConstraints(tablePane);
		wishListTableCons.setX(Spring.constant(230));
		wishListTableCons.setY(Spring.constant(250));
		tablePane.getViewport().add(wishListTable);
		
		add(tablePane);
		
		// Buy / remove
		buyButton = new JButton("Buy");
		buyButton.setFont(buttonFont);
		buyButton.setActionCommand("buy");
		buyButton.addActionListener(this);
		buyButton.setPreferredSize(new Dimension(150, 40));
		SpringLayout.Constraints buyButtonCons = mainLayout.getConstraints(buyButton);
		buyButtonCons.setX(Spring.constant(470));
		buyButtonCons.setY(Spring.constant(600));
		add(buyButton);
		
		removeButton = new JButton("Remove");
		removeButton.setFont(buttonFont);
		removeButton.setActionCommand("remove");
		removeButton.addActionListener(this);
		removeButton.setPreferredSize(new Dimension(150, 40));
		SpringLayout.Constraints removeButtonCons = mainLayout.getConstraints(removeButton);
		removeButtonCons.setX(Spring.constant(650));
		removeButtonCons.setY(Spring.constant(600));
		add(removeButton);
	}
	
	private void goToCDPanel(MusicCD cd) {
		this.setVisible(false);
		CDPanel.getInstance().setCD(cd);
		CDPanel.getInstance().setVisible(true);
	}
	
	public void updateWishListTable() {
		tableData.removeAllElements();
		for (int i = 0; i < UserController.getInstance().getLoggedInUser().getWishList().size(); i++) {
			MusicCD cd = UserController.getInstance().getLoggedInUser().getWishList().get(i);
			Vector<String> row = new Vector<String>();
			row.add(0, String.valueOf(i+1));
			row.add(1, cd.getAlbum());
			row.add(2, cd.getArtist());
			row.add(3, cd.getGenre());
			row.add(4, cd.getCostInDollars());
			
			tableData.add(row);
		}
		tablePane.getViewport().repaint();
		tablePane.getViewport().revalidate();
	}
	
	@Override
	public void actionPerformed(ActionEvent action) {
		if (action.getActionCommand().equals("home")) {
			this.setVisible(false);
			this.getParent().getComponent(0).setVisible(true);
		} else if (action.getActionCommand().equals("buy")) {
		} else if (action.getActionCommand().equals("remove")) {
			// remove selected row in wish list table
			UserController.getInstance().getLoggedInUser().getWishList().remove(wishListTable.getSelectedRow());
			updateWishListTable();
		}
	}
}
