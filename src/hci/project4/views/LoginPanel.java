package hci.project4.views;

import hci.project4.entities.LoginAuthCodes;
import hci.project4.entities.MusicCD;
import hci.project4.entities.UserController;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.Spring;
import javax.swing.SpringLayout;

@SuppressWarnings("serial")
public class LoginPanel extends JPanel implements ActionListener {
	private static LoginPanel instance = null;
	private SpringLayout mainLayout;
	private BufferedImage logoImage;
	private JLabel logoLabel, errorLabel;
	private JButton homeButton, logInButton, notRegisteredButton;
	private JLabel emailLabel, passwordLabel;
	private JTextField emailField;
	private JPasswordField passwordField;
	
	private UserController userController = UserController.getInstance();
	
	public static LoginPanel getInstance() {
		if (instance == null) {
			instance = new LoginPanel();
		}
		return instance;
	}
	
	private LoginPanel() {
		super();
		mainLayout = new SpringLayout();
		setLayout(mainLayout);
		
		Font labelFont = new Font("Verdana", Font.BOLD, 24);
		Font textFieldFont = new Font("Verdana", Font.PLAIN, 24);
		Font buttonFont = new Font("Verdana", Font.PLAIN, 20);
		Font homeButtonFont = new Font("Verdana", Font.PLAIN, 42);
		Font errorLabelFont = new Font("Verdana", Font.ITALIC, 24);
		
		//Logo
		try {
			logoImage = ImageIO.read(new File("src/images/smallLogo.png"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		logoLabel = new JLabel(new ImageIcon(logoImage));
		SpringLayout.Constraints logoLabelCons = mainLayout.getConstraints(logoLabel);
		logoLabelCons.setX(Spring.constant(30));
		logoLabelCons.setY(Spring.constant(30));
		add(logoLabel);
		
		// Home Button
		homeButton = new JButton("Home");
		homeButton.setFont(homeButtonFont);
		homeButton.setActionCommand("home");
		homeButton.addActionListener(this);
		homeButton.setPreferredSize(new Dimension(210, 80));
		SpringLayout.Constraints homeButtonCons = mainLayout.getConstraints(homeButton);
		homeButtonCons.setX(Spring.constant(1000));
		homeButtonCons.setY(Spring.constant(50));
		add(homeButton);
		
		errorLabel = new JLabel("Error Label");
		errorLabel.setFont(errorLabelFont);
		errorLabel.setForeground(Color.red);
		SpringLayout.Constraints errorLabelCons = mainLayout.getConstraints(errorLabel);
		errorLabelCons.setX(Spring.constant(560));
		errorLabelCons.setY(Spring.constant(190));
		errorLabel.setVisible(false);
		add(errorLabel);
		
		// Form
		emailLabel = new JLabel("Email");
		emailLabel.setFont(labelFont);
		SpringLayout.Constraints emailLabelCons = mainLayout.getConstraints(emailLabel);
		emailLabelCons.setX(Spring.constant(450));
		emailLabelCons.setY(Spring.constant(240));
		add(emailLabel);
		
		emailField = new JTextField(14);
		emailField.setFont(textFieldFont);
		SpringLayout.Constraints emailFieldCons = mainLayout.getConstraints(emailField);
		emailFieldCons.setX(Spring.constant(560));
		emailFieldCons.setY(Spring.constant(238));
		add(emailField);
		
		passwordLabel = new JLabel("Password");
		passwordLabel.setFont(labelFont);
		SpringLayout.Constraints passwordLabelCons = mainLayout.getConstraints(passwordLabel);
		passwordLabelCons.setX(Spring.constant(390));
		passwordLabelCons.setY(Spring.constant(290));
		add(passwordLabel);
		
		passwordField = new JPasswordField(14);
		passwordField.setFont(textFieldFont);
		SpringLayout.Constraints passwordFieldCons = mainLayout.getConstraints(passwordField);
		passwordFieldCons.setX(Spring.constant(560));
		passwordFieldCons.setY(Spring.constant(288));
		add(passwordField);
		
		//Login/Register
		logInButton = new JButton("Log In");
		logInButton.setFont(buttonFont);
		logInButton.setActionCommand("login");
		logInButton.addActionListener(this);
		logInButton.setPreferredSize(new Dimension(210, 50));
		SpringLayout.Constraints logInButtonCons = mainLayout.getConstraints(logInButton);
		logInButtonCons.setX(Spring.constant(560));
		logInButtonCons.setY(Spring.constant(350));
		add(logInButton);
		
		notRegisteredButton = new JButton("Not Registered?");
		notRegisteredButton.setFont(buttonFont);
		notRegisteredButton.setActionCommand("register");
		notRegisteredButton.addActionListener(this);
		notRegisteredButton.setPreferredSize(new Dimension(210, 35));
		SpringLayout.Constraints notRegisteredButtonCons = mainLayout.getConstraints(notRegisteredButton);
		notRegisteredButtonCons.setX(Spring.constant(560));
		notRegisteredButtonCons.setY(Spring.constant(420));
		add(notRegisteredButton);
	}
	
	@Override
	public void actionPerformed(ActionEvent action) {
		if (action.getActionCommand().equals("home")) {
			this.setVisible(false);
			this.getParent().getComponent(0).setVisible(true);
		} else if (action.getActionCommand().equals("login")) {
			System.out.println("Logging in...");
			if (!passwordField.getText().isEmpty() && !emailField.getText().isEmpty()) {
				LoginAuthCodes authCode = userController.logUserIn(emailField.getText(), passwordField.getText());
				switch (authCode) {
				case LOGIN_FAILED:
					//Not possible at the moment
					break;
				case LOGIN_SUCCESS:
					// Go to home with logout button
					errorLabel.setText("Successfully logging in..");
					errorLabel.setForeground(Color.green);
					errorLabel.setVisible(true);
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					this.setVisible(false);
					this.getParent().getComponent(0).setVisible(true);
					HomePanel.getInstance().setLoggedIn(true);
					CDPanel.getInstance().setAddToWishListButtonVisible(true);
					System.out.println("Login Successful");
					errorLabel.setText("Error Label");
					errorLabel.setForeground(Color.red);
					break;
				case LOGIN_INVALID_EMAIL:
					// Display error text saying invalid email
					System.err.println("Invalid Email");
					errorLabel.setText("Invalid Email");
					errorLabel.setVisible(true);
					break;
				case LOGIN_INVALID_PASSWORD:
					// Display error text saying invalid password
					System.err.println("Invalid Password");
					errorLabel.setText("Invalid Password");
					errorLabel.setVisible(true);
					break;
				default:
					// Something went wrong
					System.err.println("Something went wrong...");
					break;
				}
			} else {
				// Empty field
				System.err.println("Empty Field");
				errorLabel.setText("You must fill in every field");
				errorLabel.setVisible(true);
			}
		} else if (action.getActionCommand().equals("register")) {
			this.setVisible(false);
			this.getParent().getComponent(2).setVisible(true);
		}
	}
}
