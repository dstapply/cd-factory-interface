package hci.project4.views;

import hci.project4.entities.MusicCD;
import hci.project4.entities.User;
import hci.project4.entities.UserController;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.Spring;
import javax.swing.SpringLayout;

@SuppressWarnings("serial")
public class RegisterPanel extends JPanel implements ActionListener {
	private static RegisterPanel instance = null;
	private SpringLayout mainLayout;
	private BufferedImage logoImage;
	private JLabel logoLabel, errorLabel;
	private JButton homeButton, registerButton;
	private JLabel nameLabel, emailLabel, passwordLabel, confirmPasswordLabel;
	private JTextField nameField, emailField;
	private JPasswordField passwordField, confirmPasswordField;
	
	private UserController userController = UserController.getInstance();
	
	public static RegisterPanel getInstance() {
		if (instance == null) {
			instance = new RegisterPanel();
		}
		return instance;
	}
	
	public RegisterPanel() {
		super();
		mainLayout = new SpringLayout();
		setLayout(mainLayout);
		
		Font labelFont = new Font("Verdana", Font.BOLD, 24);
		Font textFieldFont = new Font("Verdana", Font.PLAIN, 24);
		Font buttonFont = new Font("Verdana", Font.PLAIN, 20);
		Font homeButtonFont = new Font("Verdana", Font.PLAIN, 42);
		Font errorLabelFont = new Font("Verdana", Font.ITALIC, 24);
		
		//Logo
		try {
			logoImage = ImageIO.read(new File("src/images/smallLogo.png"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		logoLabel = new JLabel(new ImageIcon(logoImage));
		SpringLayout.Constraints logoLabelCons = mainLayout.getConstraints(logoLabel);
		logoLabelCons.setX(Spring.constant(30));
		logoLabelCons.setY(Spring.constant(30));
		add(logoLabel);
		
		// Home Button
		homeButton = new JButton("Home");
		homeButton.setFont(homeButtonFont);
		homeButton.setActionCommand("home");
		homeButton.addActionListener(this);
		homeButton.setPreferredSize(new Dimension(210, 80));
		SpringLayout.Constraints homeButtonCons = mainLayout.getConstraints(homeButton);
		homeButtonCons.setX(Spring.constant(1000));
		homeButtonCons.setY(Spring.constant(50));
		add(homeButton);
		
		errorLabel = new JLabel("Error Label");
		errorLabel.setFont(errorLabelFont);
		errorLabel.setForeground(Color.red);
		SpringLayout.Constraints errorLabelCons = mainLayout.getConstraints(errorLabel);
		errorLabelCons.setX(Spring.constant(560));
		errorLabelCons.setY(Spring.constant(190));
		errorLabel.setVisible(false);
		add(errorLabel);
		
		// Form
		nameLabel = new JLabel("Name");
		nameLabel.setFont(labelFont);
		SpringLayout.Constraints nameLabelCons = mainLayout.getConstraints(nameLabel);
		nameLabelCons.setX(Spring.constant(450));
		nameLabelCons.setY(Spring.constant(240));
		add(nameLabel);
		
		nameField = new JTextField(14);
		nameField.setFont(textFieldFont);
		SpringLayout.Constraints nameFieldCons = mainLayout.getConstraints(nameField);
		nameFieldCons.setX(Spring.constant(560));
		nameFieldCons.setY(Spring.constant(238));
		add(nameField);
		
		emailLabel = new JLabel("Email");
		emailLabel.setFont(labelFont);
		SpringLayout.Constraints emailLabelCons = mainLayout.getConstraints(emailLabel);
		emailLabelCons.setX(Spring.constant(453));
		emailLabelCons.setY(Spring.constant(290));
		add(emailLabel);
		
		emailField = new JTextField(14);
		emailField.setFont(textFieldFont);
		SpringLayout.Constraints emailFieldCons = mainLayout.getConstraints(emailField);
		emailFieldCons.setX(Spring.constant(560));
		emailFieldCons.setY(Spring.constant(288));
		add(emailField);
		
		passwordLabel = new JLabel("Password");
		passwordLabel.setFont(labelFont);
		SpringLayout.Constraints passwordLabelCons = mainLayout.getConstraints(passwordLabel);
		passwordLabelCons.setX(Spring.constant(395));
		passwordLabelCons.setY(Spring.constant(340));
		add(passwordLabel);
		
		passwordField = new JPasswordField(14);
		passwordField.setFont(textFieldFont);
		SpringLayout.Constraints passwordFieldCons = mainLayout.getConstraints(passwordField);
		passwordFieldCons.setX(Spring.constant(560));
		passwordFieldCons.setY(Spring.constant(338));
		add(passwordField);
		
		confirmPasswordLabel = new JLabel("Confirm Password");
		confirmPasswordLabel.setFont(labelFont);
		SpringLayout.Constraints confirmPasswordLabelCons = mainLayout.getConstraints(confirmPasswordLabel);
		confirmPasswordLabelCons.setX(Spring.constant(281));
		confirmPasswordLabelCons.setY(Spring.constant(390));
		add(confirmPasswordLabel);
		
		confirmPasswordField = new JPasswordField(14);
		confirmPasswordField.setFont(textFieldFont);
		SpringLayout.Constraints confirmPasswordFieldCons = mainLayout.getConstraints(confirmPasswordField);
		confirmPasswordFieldCons.setX(Spring.constant(560));
		confirmPasswordFieldCons.setY(Spring.constant(388));
		add(confirmPasswordField);
		
		//Register
		registerButton = new JButton("Register");
		registerButton.setFont(buttonFont);
		registerButton.setActionCommand("register");
		registerButton.addActionListener(this);
		registerButton.setPreferredSize(new Dimension(210, 50));
		SpringLayout.Constraints registerButtonCons = mainLayout.getConstraints(registerButton);
		registerButtonCons.setX(Spring.constant(560));
		registerButtonCons.setY(Spring.constant(440));
		add(registerButton);
	}
	
	@Override
	public void actionPerformed(ActionEvent action) {
		if (action.getActionCommand().equals("home")) {
			this.setVisible(false);
			this.getParent().getComponent(0).setVisible(true);
		} else if (action.getActionCommand().equals("register")) {
			if (!nameField.getText().isEmpty() && !emailField.getText().isEmpty() 
					&& !passwordField.getPassword().toString().isEmpty() 
					&& !confirmPasswordField.getPassword().toString().isEmpty()) {
				if (passwordField.getText().equals(confirmPasswordField.getText())) {
					User newUser = new User(nameField.getText(), emailField.getText(), passwordField.getText(), true);
					if (!userController.isUserRegistered(newUser.getEmail())) {
						errorLabel.setText("Error Label");
						errorLabel.setVisible(false);
						userController.registerUser(newUser);
						if (userController.isAUserLoggedIn()) {
							userController.unLogUser();
						}
						userController.logUserIn(newUser.getEmail(), newUser.getPassword());
						this.setVisible(false);
						HomePanel.getInstance().setLoggedIn(true);
						this.getParent().getComponent(0).setVisible(true);
						CDPanel.getInstance().setAddToWishListButtonVisible(true);
					} else {
						//user already registered
						System.err.println("Email already registered");
						errorLabel.setText("Email is already registered");
						errorLabel.setVisible(true);
					}
				} else {
					// confirm password not valid
					System.err.println("Passwords did not match");
					errorLabel.setText("Passwords did not match");
					errorLabel.setVisible(true);
				}
			} else {
				// Empty Field
				System.err.println("Empty Field");
				errorLabel.setText("You must fill in every field");
				errorLabel.setVisible(true);
			}
		}
	}
}
